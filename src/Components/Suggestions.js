import React from 'react'
import classes from './Suggestions.module.css'

const Suggestions = (props) => {

  const options = props.results.map(r => (
    <li
      key={r.id}
      onClick={() => props.clicked(r.name, r.code)}>
      {r.name}
    </li>
  ))
  return <ul className={classes.Suggestions}>{options}</ul>
}

export default Suggestions
