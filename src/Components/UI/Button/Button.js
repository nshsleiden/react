import React from 'react'
import classes from './Button.module.css'

const button = (props) => (    
    <button
        disabled={props.disabled}
        className={[classes.Button, classes[props.variant]].join(' ')}
        style={props.custom}
        onClick={props.clicked}>{props.children}</button>   
    
)

export default button