import React from  'react'

import Layout from '../../hoc/Layout'
import Logo from '../../Components/Logo'

const thanks = () => {
    return (
        <Layout background>
            <Logo />
                <h1 className="title">Bedankt voor uw melding</h1>
                <h2>We zullen er zo snel mogelijk een conducteur op af sturen.</h2>
        </Layout>
    )
}

export default thanks;