import axios from 'axios';
// import Auth from './hoc/Auth'

const instance = axios.create({
    baseURL: 'https://api.nsklacht.nl/api'
});

instance.interceptors.request.use(request => {
    // const token = Auth.getToken()
    
    const tokenA = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImMwZmQxNzg3OTkxZTIwYWU0Y2I1OTYwMThhYmRkYTQ3NTgzZGJkZWFiMDk4ZTIxNTYyMmZjOGYwYmY1OTdlNTJiYWJhMjQ2MzU0ZWZjZTdiIn0.eyJhdWQiOiIxIiwianRpIjoiYzBmZDE3ODc5OTFlMjBhZTRjYjU5NjAxOGFiZGRhNDc1ODNkYmRlYWIwOThlMjE1NjIyZmM4ZjBiZjU5N2U1MmJhYmEyNDYzNTRlZmNlN2IiLCJpYXQiOjE1NjE1NTUxMDIsIm5iZiI6MTU2MTU1NTEwMiwiZXhwIjoxNTkzMTc3NTAyLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.bH2mDqLhnZk9I7EkU2gx04X3V4MyrpYL_pjGNIiIOehiCKUuG19bqOEqEc1v40rEtjg4m1vYrkfsP3DPI8AhXEvDnhLAwUyPVqEadSayRkcfsv5BxllpFsylixsze3sLHoOdYxyJXXOU-OcZCIgc8aJYQC__EAgMyXkkrAxcEdg0tSRgJ04_n3L8kQjly0RUPfu1DdQuFPvO593IH2S1_MKDo4sY8QbClbhF9pyEAgDGjJmiDnmwt5etG_1dst9rwN_8C-ruG-oFFnoE9RBFwaYvXLhSWTSFdSTX91NBB4LW2uQnpSJC9yg3nY78hCmLga-JE5lOO4IOFnp23wxVfLlkL6QlbS3AHLzTxZHaWCNEZrEkN5j3jquEzUptlru8CKKMG9ewTTZyTUbR0VofkY2Ig4NXc8iBaTW0-g_8c3TJAlc1Mwfzf6tri8PFhu1NPuw0QEqG9luIKak5misDBMwXh7jZDw2IamB3ZwGEgJx5KVMzsD9Q-bPsDR-rZTZfMshx7ZG5M50rro-0EV3uq9l7zK7erLhU36WsxsR0XwLsFWcI4L-D6YbAYWsn6M2Jj0rodBp-YmJL1DfyNjiwI_kBlEDTbh2tEIjFWINANu0NC8iyJGZxKVuAPCG7ewjXg738bPrtjn-D9k5LmUc30Q-4cm9E6ndzebxKqgKMPtA"
    request.headers.common = { 'Authorization': `Bearer ${tokenA}` }

    return request
})

export default instance;